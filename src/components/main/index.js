import React from 'react';
import Title from '../title';
import AddTask from '../add-task';

import './main.css'

const Main = ({taskArr}) => {
    return (
        <div className='main_div'>
            <Title text='Список задач на сегодня'> </Title>
            <AddTask taskArr={taskArr}></AddTask>
            <Title text='Осталось __ задач'> </Title>
        </div>
    )
}

export default Main;
